function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}

$(function () {
	$('.my-img').click(function() {
		$('.myprofile-box').slideToggle();
	})
});
 /*Upload avatar -----------------------------------*/ 
$(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
});
/*end----------------------------------------*/

$(document).ready(function() {
	$("#reg_bb").click(function(){
    	$('#modal_1').modal('toggle');
    	$('#register_model').modal('show');
	});
});	

//mobile menu *****************************************///
          var nav = document.querySelector("nav");

        // window.onscroll =  function (){
        //     if (window.scrollY >= 100){
        //         nav.style.position = "fixed";
        //         nav.style.top = "0px";
        //         nav.style.left = "0px";
        //     } else {
        //         nav.style.position = "";
        //     }
        // };

        var sidenav_btn = document.querySelector("#sidenav-btn");

        sidenav_btn.addEventListener("click", function (){
            var sidenav = document.querySelector(".sidenav");

            sidenav.style.transform = "translateX(0px)";

            var black = document.querySelector(".black");

            black.style.display = "block";
        });

        var black = document.querySelector(".black");

        black.addEventListener("click", function (){
            var sidenav = document.querySelector(".sidenav");

            sidenav.style.transform = "translateX(-250px)";

            this.style.display = "none";
        });
// end ***************************************************//

// ---------------------------------------------------------

$(document).ready(function(){
    $("#drop-toggle").click(function(){
        $('ul#contentUsers').toggle(500 ,function(){
            // $('.fa-caret-down').css({"transform": "rotate(180deg)", "transition": "0.5s"});
        });
    });
});

// --------------------------------------------------------

// scroll
    $(document).ready(function(){
        $(window).scroll(function() {
          if ($(this).scrollTop() > 100) {
                $('.scroll-to-top').fadeIn();
            } else {
              $('.scroll-to-top').fadeOut();
            }
        });

        $('.scroll-to-top').on('click', function(e) {
          e.preventDefault();
            $('html, body').animate({scrollTop : 0}, 800);
        });
    });
// scroll end 